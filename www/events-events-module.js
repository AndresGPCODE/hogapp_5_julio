(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["events-events-module"],{

/***/ "./src/app/pages/private/events/attend/attend.page.html":
/*!**************************************************************!*\
  !*** ./src/app/pages/private/events/attend/attend.page.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Registrarme</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-list>\n    <ion-item>\n      <ion-label>¿Con Moto?</ion-label>\n      <ion-toggle [(ngModel)]=\"attend.ConMoto\"></ion-toggle>\n    </ion-item>\n  \n    <ion-item>\n      <ion-label>¿Con Acompañante?</ion-label>\n      <ion-toggle [(ngModel)]=\"attend.ConAcompanante\"></ion-toggle>\n    </ion-item>\n\n    <ion-item *ngIf=\"attend.ConAcompanante\">\n      <ion-input name=\"acomp\" type=\"text\" #acomp=\"ngModel\" placeholder=\"Nombre del Acompañante\" [(ngModel)]=\"attend.NombreAcompanante\"></ion-input>\n    </ion-item>\n  \n    <ion-item>\n      <ion-label>¿Con Invitado?</ion-label>\n      <ion-toggle [(ngModel)]=\"attend.ConInvitado\"></ion-toggle>\n    </ion-item>\n\n    <ion-item *ngIf=\"attend.ConInvitado\">\n      <ion-input name=\"inv\" type=\"text\" #inv=\"ngModel\" placeholder=\"Nombre del Invitado\" [(ngModel)]=\"attend.NombreInvitado\"></ion-input>\n    </ion-item>\n  </ion-list>\n\n  <ion-button expand=\"block\" (click)=\"register(attend)\">Registrarme</ion-button>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/private/events/attend/attend.page.scss":
/*!**************************************************************!*\
  !*** ./src/app/pages/private/events/attend/attend.page.scss ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ByaXZhdGUvZXZlbnRzL2F0dGVuZC9hdHRlbmQucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/private/events/attend/attend.page.ts":
/*!************************************************************!*\
  !*** ./src/app/pages/private/events/attend/attend.page.ts ***!
  \************************************************************/
/*! exports provided: AttendPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AttendPage", function() { return AttendPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_events_events_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/events/events.service */ "./src/app/services/events/events.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_7__);








var AttendPage = /** @class */ (function () {
    function AttendPage(eventService, alertController, toastController, router, route, storage, navCtrl, geolocation) {
        this.eventService = eventService;
        this.alertController = alertController;
        this.toastController = toastController;
        this.router = router;
        this.route = route;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.geolocation = geolocation;
        this.attend = {};
    }
    AttendPage.prototype.ngOnInit = function () {
        var _this = this;
        this.attend.ConMoto = false;
        this.attend.ConAcompanante = false;
        this.attend.ConInvitado = false;
        this.attend.NombreAcompanante = '';
        this.attend.NombreInvitado = '';
        this.storage.get('user').then(function (usr) {
            var decrypted = JSON.parse(crypto_js__WEBPACK_IMPORTED_MODULE_7__["AES"].decrypt(usr, " ", { format: CryptoJSAesJson }).toString(crypto_js__WEBPACK_IMPORTED_MODULE_7__["enc"].Utf8));
            if (usr) {
                _this.attend.nIdUsuario = decrypted.nIDUsuario;
            }
            else {
                _this.navCtrl.back();
            }
        });
        this.attend.nIdEvento = Number(this.route.snapshot.paramMap.get('id'));
        this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(function (resp) {
            _this.attend.Latitud = resp.coords.latitude.toString();
            _this.attend.Longitud = resp.coords.longitude.toString();
            _this.attend.Altitud = resp.coords.altitude ? resp.coords.altitude.toString() : null;
        }, function (err) {
            console.log('ERROR GEOLOCATION', err);
            _this.presentToast('No se pudo obtener tu localización.');
            _this.navCtrl.back();
        });
    };
    AttendPage.prototype.register = function (attend) {
        var _this = this;
        this.presentAlertConfirm('Registrarme', '¿Registrarse en este evento?', [
            {
                text: 'Cancelar',
                role: 'cancel',
                cssClass: 'secondary'
            }, {
                text: 'Registrarme',
                handler: function () {
                    _this.eventService.attend(attend).subscribe(function (resp) {
                        if (resp.status) {
                            console.log('ATTEND', resp);
                            _this.presentToast('¡Has sido registrado en el evento!');
                            _this.router.navigate(['private', 'events'], { replaceUrl: true });
                        }
                        else {
                            console.log('ATTEND ERROR', resp);
                            _this.presentToast(resp.message);
                        }
                    }, function (err) {
                        console.error('SERVER ERROR ATTEND', err);
                        _this.presentToast('Error de Conexión');
                    });
                }
            }
        ]);
    };
    AttendPage.prototype.presentToast = function (text) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: text,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    AttendPage.prototype.presentAlertConfirm = function (header, message, buttons) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var alert;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            header: header,
                            message: message,
                            buttons: buttons
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    AttendPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-attend',
            template: __webpack_require__(/*! ./attend.page.html */ "./src/app/pages/private/events/attend/attend.page.html"),
            styles: [__webpack_require__(/*! ./attend.page.scss */ "./src/app/pages/private/events/attend/attend.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_events_events_service__WEBPACK_IMPORTED_MODULE_2__["EventsService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["AlertController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["NavController"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__["Geolocation"]])
    ], AttendPage);
    return AttendPage;
}());



/***/ }),

/***/ "./src/app/pages/private/events/details/details.page.html":
/*!****************************************************************!*\
  !*** ./src/app/pages/private/events/details/details.page.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-back-button></ion-back-button>\n    </ion-buttons>\n    <ion-title>Evento</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div style=\"text-align:center;\">\n    <h2>{{ event.Nombre }}</h2>\n    <h3>{{ event.LugarOrigen + ' - ' + event.LugarDestino }}</h3>\n    <p>{{ event.FechaInicio + ' al ' + event.FechaFin }}</p>\n  </div>\n  <div *ngIf=\"location != '0,0'\">\n    <img [src]=\"'https://maps.googleapis.com/maps/api/staticmap?center=' + location + '&zoom=15&size=600x300&maptype=roadmap&markers=color:red%7Clabel:C%7C' + location + '&key=' + mapsKey\">\n  </div>\n  <div padding>\n    <ion-button expand=\"block\" (click)=\"register()\">Registra mi Asistencia</ion-button>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/private/events/details/details.page.scss":
/*!****************************************************************!*\
  !*** ./src/app/pages/private/events/details/details.page.scss ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ByaXZhdGUvZXZlbnRzL2RldGFpbHMvZGV0YWlscy5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/private/events/details/details.page.ts":
/*!**************************************************************!*\
  !*** ./src/app/pages/private/events/details/details.page.ts ***!
  \**************************************************************/
/*! exports provided: DetailsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetailsPage", function() { return DetailsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_config_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_events_events_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/events/events.service */ "./src/app/services/events/events.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");







var DetailsPage = /** @class */ (function () {
    function DetailsPage(configService, geolocation, toastController, loading, eventService, route, router) {
        this.configService = configService;
        this.geolocation = geolocation;
        this.toastController = toastController;
        this.loading = loading;
        this.eventService = eventService;
        this.route = route;
        this.router = router;
        this.event = {};
        this.mapsKey = this.configService.mapsKey();
        this.location = '0,0';
    }
    DetailsPage.prototype.ngOnInit = function () {
        var _this = this;
        console.log('ID EVENT', this.route.snapshot.paramMap.get('id'));
        this.eventService.getById(this.route.snapshot.paramMap.get('id')).subscribe(function (resp) {
            if (resp.status) {
                console.log('GET EVENT', resp);
                _this.event = resp.message;
            }
            else {
                console.error('ERROR GET EVENT', resp);
                _this.presentToast('Error al Obtener el evento, intentalo de nuevo.');
                _this.router.navigate(['private', 'events']);
            }
        }, function (err) {
            console.error('SERVER ERROR GETTING EVENT', err);
            _this.presentToast('Sin Conexión, intentalo más tarde.');
            _this.router.navigate(['private', 'events']);
        });
        this.geolocation.getCurrentPosition().then(function (resp) {
            _this.location = resp.coords.latitude + ',' + resp.coords.longitude;
        }, function (err) {
            console.error('GETTING GEOLOCATION', err);
            _this.presentToast('No se puede obtener tu ubicación.');
        });
    };
    DetailsPage.prototype.register = function () {
        this.router.navigate(['private', 'events', 'attend', this.event.nIdEvento]);
    };
    DetailsPage.prototype.presentToast = function (text) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: text,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    DetailsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-details',
            template: __webpack_require__(/*! ./details.page.html */ "./src/app/pages/private/events/details/details.page.html"),
            styles: [__webpack_require__(/*! ./details.page.scss */ "./src/app/pages/private/events/details/details.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_config_service__WEBPACK_IMPORTED_MODULE_2__["ConfigService"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_3__["Geolocation"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["LoadingController"],
            src_app_services_events_events_service__WEBPACK_IMPORTED_MODULE_5__["EventsService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_6__["Router"]])
    ], DetailsPage);
    return DetailsPage;
}());



/***/ }),

/***/ "./src/app/pages/private/events/events.module.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/private/events/events.module.ts ***!
  \*******************************************************/
/*! exports provided: EventsPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsPageModule", function() { return EventsPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _events_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./events.page */ "./src/app/pages/private/events/events.page.ts");
/* harmony import */ var _details_details_page__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./details/details.page */ "./src/app/pages/private/events/details/details.page.ts");
/* harmony import */ var _attend_attend_page__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./attend/attend.page */ "./src/app/pages/private/events/attend/attend.page.ts");









var routes = [
    {
        path: '',
        component: _events_page__WEBPACK_IMPORTED_MODULE_6__["EventsPage"]
    },
    {
        path: 'detail/:id',
        component: _details_details_page__WEBPACK_IMPORTED_MODULE_7__["DetailsPage"]
    },
    {
        path: 'attend/:id',
        component: _attend_attend_page__WEBPACK_IMPORTED_MODULE_8__["AttendPage"]
    }
];
var EventsPageModule = /** @class */ (function () {
    function EventsPageModule() {
    }
    EventsPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_events_page__WEBPACK_IMPORTED_MODULE_6__["EventsPage"], _details_details_page__WEBPACK_IMPORTED_MODULE_7__["DetailsPage"], _attend_attend_page__WEBPACK_IMPORTED_MODULE_8__["AttendPage"]]
        })
    ], EventsPageModule);
    return EventsPageModule;
}());



/***/ }),

/***/ "./src/app/pages/private/events/events.page.html":
/*!*******************************************************!*\
  !*** ./src/app/pages/private/events/events.page.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-toggle>\n        <ion-button>\n          <ion-icon slot=\"icon-only\" name=\"menu\"></ion-icon>\n        </ion-button>\n      </ion-menu-toggle>\n    </ion-buttons>\n    <ion-title>Eventos</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <ion-refresher slot=\"fixed\" (ionRefresh)=\"doRefresh($event)\">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n  <ion-card *ngFor=\"let ev of orderBy('FechaInicio')\">\n    <ion-card-header>\n      <ion-card-title>{{ ev.Nombre }}</ion-card-title>\n      <ion-card-subtitle>{{ ev.NumeroHoras }} horas</ion-card-subtitle>\n    </ion-card-header>\n\n    <ion-item>\n      <ion-icon name=\"calendar\" slot=\"start\"></ion-icon>\n      <ion-label>{{ ev.FechaInicio }}</ion-label>\n    </ion-item>\n\n    <ion-item>\n      <ion-icon name=\"pin\" slot=\"start\"></ion-icon>\n      <ion-label>{{ ev.LugarOrigen }}</ion-label>\n      <ion-button (click)=\"viewDetails(ev.nIdEvento)\" fill=\"outline\" slot=\"end\">View</ion-button>\n    </ion-item>\n  \n    <ion-card-content>\n      {{ ev.Descripcion }}\n    </ion-card-content>\n  </ion-card>\n\n  <div padding class=\"no-event\" *ngIf=\"events.length == 0\">\n    <h2><ion-icon name=\"calendar\"></ion-icon></h2>\n    <p>No hay Eventos próximos</p>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/private/events/events.page.scss":
/*!*******************************************************!*\
  !*** ./src/app/pages/private/events/events.page.scss ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".no-event {\n  background-color: #dedede;\n  text-align: center; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy91c3VhcmlvL0Rlc2t0b3AvSG9nQXBwXzI3X0pVTklPXzIwMTkvc3JjL2FwcC9wYWdlcy9wcml2YXRlL2V2ZW50cy9ldmVudHMucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQXlCO0VBQ3pCLGtCQUFrQixFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHJpdmF0ZS9ldmVudHMvZXZlbnRzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5uby1ldmVudHtcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZGVkZWRlO1xuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcbn0iXX0= */"

/***/ }),

/***/ "./src/app/pages/private/events/events.page.ts":
/*!*****************************************************!*\
  !*** ./src/app/pages/private/events/events.page.ts ***!
  \*****************************************************/
/*! exports provided: EventsPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsPage", function() { return EventsPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_events_events_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/events/events.service */ "./src/app/services/events/events.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var EventsPage = /** @class */ (function () {
    function EventsPage(eventsService, toastController, router) {
        this.eventsService = eventsService;
        this.toastController = toastController;
        this.router = router;
        this.events = new Array();
    }
    EventsPage.prototype.ngOnInit = function () {
        this.doRefresh();
    };
    EventsPage.prototype.doRefresh = function (event) {
        var _this = this;
        this.eventsService.get().subscribe(function (resp) {
            if (resp.status) {
                console.log('GET EVENTS', resp);
                _this.events = resp.message;
            }
            else {
                console.error('GET EVENTS ERROR', resp);
                _this.presentToast('Error al obtener los Eventos.');
            }
            if (event) {
                event.target.complete();
            }
        }, function (err) {
            console.error('GET EVENTS SERVER ERROR', err);
            _this.presentToast('Error de Conexión');
            if (event) {
                event.target.complete();
            }
        });
    };
    EventsPage.prototype.viewDetails = function (id) {
        this.router.navigate(['private', 'events', 'detail', id]);
    };
    EventsPage.prototype.orderBy = function (prop) {
        return this.events.sort(function (a, b) { return a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1; });
    };
    EventsPage.prototype.presentToast = function (text) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: text,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    EventsPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-events',
            template: __webpack_require__(/*! ./events.page.html */ "./src/app/pages/private/events/events.page.html"),
            styles: [__webpack_require__(/*! ./events.page.scss */ "./src/app/pages/private/events/events.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_events_events_service__WEBPACK_IMPORTED_MODULE_2__["EventsService"], _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["ToastController"], _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], EventsPage);
    return EventsPage;
}());



/***/ }),

/***/ "./src/app/services/events/events.service.ts":
/*!***************************************************!*\
  !*** ./src/app/services/events/events.service.ts ***!
  \***************************************************/
/*! exports provided: EventsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EventsService", function() { return EventsService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_4__);





var EventsService = /** @class */ (function () {
    function EventsService(http, config) {
        this.http = http;
        this.config = config;
    }
    EventsService.prototype.get = function () {
        return this.http.get(this.config.host().concat('accountEventos.php?bEstado=1&Today=1'), {});
    };
    EventsService.prototype.getById = function (id) {
        return this.http.get(this.config.host().concat('accountEventos.php?bEstado=1&id=', id), {});
    };
    EventsService.prototype.attend = function (attend) {
        console.log('TO ATTEND', attend);
        var encrypted = crypto_js__WEBPACK_IMPORTED_MODULE_4__["AES"].encrypt(JSON.stringify({ attend: attend }), " ", { format: CryptoJSAesJson }).toString();
        console.log('TO ATTEND encrypted', encrypted);
        return this.http.post(this.config.host().concat('asistencias.php'), encrypted, {});
        // return this.http.post(this.config.host().concat('asistencias.php'), JSON.stringify({attend}), {});
    };
    EventsService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]])
    ], EventsService);
    return EventsService;
}());



/***/ })

}]);
//# sourceMappingURL=events-events-module.js.map