namespace Incidents {
    export interface IncidentInterface {
        nIdIncidente: number;
        IdUsuario: number;
        Tipo: string;
        FechaRegistro: Date;
        FechaModificacion: Date;
        Observaciones: string;
        bEstado: number;
        action: string;
    }

    export interface IncidentLocationInterface {
        nIdIncidenteUbicacion: number;
        IdIncidente: number;
        Latitud: number;
        Longitud: number;
        FechaRegistro: Date;
        FechaModificacion: Date;
        Observaciones: string;
        bEstado: number;
        action: string;
    }
}
