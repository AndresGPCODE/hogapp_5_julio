namespace Events {
    export interface EventInterface {
        nIdEvento: number;
        nIdTipoEvento: number;
        Nombre: string;
        Descripcion: string;
        FechaInicio: Date;
        FechaFin: Date;
        NumeroDias: number;
        NumeroHoras: number;
        LimiteParticipantes: number;
        LugarOrigen: string;
        LugarDestino: string;
        KM: number;
        Caracteristicas: string;
        TipoRiesgo: string;
        Nacional: string;
        FechaCreacion: Date;
        FechaModificacion: Date;
        Observaciones: string;
        bEstado: number;
    }

    export interface AttendInterface {
        nIdAsistencia: number;
        nIdUsuario: number;
        nIdEvento: number;
        ConMoto: boolean;
        ConAcompanante: boolean;
        NombreAcompanante: string;
        ConInvitado: boolean;
        NombreInvitado: string;
        FechaRegistro: Date;
        UbicacionRegistro: string;
        Latitud: string;
        Longitud: string;
        Altitud: string;
        Aceptacion: boolean;
        FechaCreacion: Date;
        FechaModificacion: Date;
        Observaciones: string;
        bEstado: number;
    }
}
