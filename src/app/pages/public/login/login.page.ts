import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/services/auth/authentication.service';
import { ToastController, Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { FcmService } from 'src/app/services/firebase/fcm.service';
import * as CryptoJS from 'crypto-js';
declare var CryptoJSAesJson;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  user = {
    email: '',
    password: ''
  };


  constructor(private router: Router,
    private authService: AuthenticationService,
    private toastController: ToastController,
    private storage: Storage,
    private fcm: FcmService,
    private platform: Platform) {

  }

  ngOnInit() {
  }

  login(user) {
    this.authService.login(user.email, user.password).subscribe(resp => {
      
      if (resp.status) {
          //encriptacion de los datos de sesion
          resp.data={'Alias':         resp.data['Alias'],
                     'Nombre':        resp.data['Nombre'],
                     'Password':      resp.data['Password'],
                     'nIDUsuario':    resp.data['nIDUsuario'],
                     'nIDMotoClub':   resp.data['nIDMotoClub  ']
                    };
          var encrypted = CryptoJS.AES.encrypt(JSON.stringify(resp.data), " ", {format: CryptoJSAesJson}).toString();
          this.storage.set('user', encrypted);
          var decrypted = JSON.parse(CryptoJS.AES.decrypt(encrypted, " ", {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
          //---------------
        this.presentToast('¡Bienvenido '.concat(decrypted.Nombre, '!'));
        this.notificationSetup(decrypted.nIDUsuario);
        this.router.navigate(['private', 'dashboard'], {replaceUrl: true});
        
      } else {
        console.log('LOGIN FAIL', resp);
        this.presentToast(resp.message);
      }
    }, err => {
      console.error('ERROR LOGIN', err);
      this.presentToast('Error de conexion');
    });
  }

  private notificationSetup(idUser) {
    this.fcm.getToken(idUser);
    this.fcm.onNotifications().subscribe(
      (msg) => {
        if (this.platform.is('ios')) {
          this.presentToast(msg.aps.alert);
        } else {
          this.presentToast(msg.body);
        }
      });
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }
}