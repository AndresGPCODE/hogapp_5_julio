import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PublicPagePage } from './public-page.page';
import { PublicPagesRoutingModule } from './public-pages-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PublicPagesRoutingModule
  ],
  declarations: [PublicPagePage]
})
export class PublicPagePageModule {}
