import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'private/dashboard' },
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
  { path: 'events', loadChildren: './events/events.module#EventsPageModule' },
  { path: 'configuration', loadChildren: './configuration/configuration.module#ConfigurationPageModule' },
  { path: 'vehicle', loadChildren: './vehicle/vehicle.module#VehiclePageModule' },
  { path: 'medical', loadChildren: './medical/medical.module#MedicalPageModule' },
  { path: 'incident/:tipo', loadChildren: './incident/incident.module#IncidentPageModule'}
  // { path: 'attend', loadChildren: './events/attend/attend.module#AttendPageModule' },
  // { path: 'details', loadChildren: './events/details/details.module#DetailsPageModule' }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class PrivatePagesRoutingModule { }
