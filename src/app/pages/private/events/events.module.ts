import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { IonicModule } from '@ionic/angular';
import { EventsPage } from './events.page';
import { DetailsPage } from './details/details.page';
import { AttendPage } from './attend/attend.page';

const routes: Routes = [
  {
    path: '',
    component: EventsPage
  },
  {
    path: 'detail/:id',
    component: DetailsPage
  },
  {
    path: 'attend/:id',
    component: AttendPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [EventsPage, DetailsPage, AttendPage]
})
export class EventsPageModule {}
