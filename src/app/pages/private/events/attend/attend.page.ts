import { Component, OnInit } from '@angular/core';
import { EventsService } from 'src/app/services/events/events.service';
import { AlertController, ToastController, NavController } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import * as CryptoJS from 'crypto-js';
declare var CryptoJSAesJson;
@Component({
  selector: 'app-attend',
  templateUrl: './attend.page.html',
  styleUrls: ['./attend.page.scss'],
})
export class AttendPage implements OnInit {

  public attend: Events.AttendInterface;

  constructor(private eventService: EventsService,
    private alertController: AlertController,
    private toastController: ToastController,
    private router: Router,
    private route: ActivatedRoute,
    private storage: Storage,
    private navCtrl: NavController,
    private geolocation: Geolocation) {
      this.attend = {} as Events.AttendInterface;
  }

  ngOnInit() {
    this.attend.ConMoto = false;
    this.attend.ConAcompanante = false;
    this.attend.ConInvitado = false;
    this.attend.NombreAcompanante = '';
    this.attend.NombreInvitado = '';

    this.storage.get('user').then(usr => {
      
      var decrypted = JSON.parse(CryptoJS.AES.decrypt(usr, " ", {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
      if (usr) {
        this.attend.nIdUsuario = decrypted.nIDUsuario;
      } else {
        this.navCtrl.back();
      }
    });

    this.attend.nIdEvento = Number(this.route.snapshot.paramMap.get('id'));
    this.geolocation.getCurrentPosition({ enableHighAccuracy: true }).then(resp => {
      this.attend.Latitud = resp.coords.latitude.toString();
      this.attend.Longitud = resp.coords.longitude.toString();
      this.attend.Altitud = resp.coords.altitude ? resp.coords.altitude.toString() : null;
    }, err => {
      console.log('ERROR GEOLOCATION', err);
      this.presentToast('No se pudo obtener tu localización.');
      this.navCtrl.back();
    });
  }

  register(attend) {
    this.presentAlertConfirm('Registrarme', '¿Registrarse en este evento?', [
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary'
      }, {
        text: 'Registrarme',
        handler: () => {
          this.eventService.attend(attend).subscribe(resp => {
            if (resp.status) {
              console.log('ATTEND', resp);
              this.presentToast('¡Has sido registrado en el evento!');
              this.router.navigate(['private', 'events'], { replaceUrl: true });
            } else {
              console.log('ATTEND ERROR', resp);
              this.presentToast(resp.message);
            }
          }, err => {
            console.error('SERVER ERROR ATTEND', err);
            this.presentToast('Error de Conexión');
          });
        }
      }
    ]);
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

  async presentAlertConfirm(header: string, message: string, buttons: Array<any>) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: buttons
    });

    await alert.present();
  }

}
