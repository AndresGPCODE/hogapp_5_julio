import { Component, OnInit } from '@angular/core';
import { ConfigService } from 'src/app/services/config.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { ToastController, LoadingController } from '@ionic/angular';
import { EventsService } from 'src/app/services/events/events.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-details',
  templateUrl: './details.page.html',
  styleUrls: ['./details.page.scss'],
})
export class DetailsPage implements OnInit {

  public mapsKey: string;
  public location: string;
  public event: Events.EventInterface;

  constructor(private configService: ConfigService,
    private geolocation: Geolocation,
    private toastController: ToastController,
    private loading: LoadingController,
    private eventService: EventsService,
    private route: ActivatedRoute,
    private router: Router) {
      this.event = {} as Events.EventInterface;
      this.mapsKey = this.configService.mapsKey();
      this.location = '0,0';
  }

  ngOnInit() {
    this.eventService.getById(this.route.snapshot.paramMap.get('id')).subscribe(resp => {
      if (resp.status) {
        console.log('GET EVENT', resp);
        this.event = resp.message;
      } else {
        console.error('ERROR GET EVENT', resp);
        this.presentToast('Error al Obtener el evento, intentalo de nuevo.');
        this.router.navigate(['private', 'events']);
      }
    }, err => {
      console.error('SERVER ERROR GETTING EVENT', err);
      this.presentToast('Sin Conexión, intentalo más tarde.');
      this.router.navigate(['private', 'events']);
    });

    this.geolocation.getCurrentPosition().then(resp => {
      this.location = resp.coords.latitude + ',' + resp.coords.longitude;
    }, err => {
      console.error('GETTING GEOLOCATION', err);
      this.presentToast('No se puede obtener tu ubicación.');
    });
  }

  register() {
    this.router.navigate(['private', 'events', 'attend', this.event.nIdEvento]);
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

}
