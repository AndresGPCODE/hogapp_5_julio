import { Component, OnInit } from '@angular/core';
import { EventsService } from 'src/app/services/events/events.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-events',
  templateUrl: './events.page.html',
  styleUrls: ['./events.page.scss'],
})
export class EventsPage implements OnInit {

  public events: Array<Events.EventInterface>;

  constructor(private eventsService: EventsService, private toastController: ToastController, private router: Router) {
    this.events = new Array<Events.EventInterface>();
  }

  ngOnInit() {
    this.doRefresh();
  }

  doRefresh(event?) {
    this.eventsService.get().subscribe(resp => {
      if (resp.status) {
        console.log('GET EVENTS', resp);
        this.events = resp.message;
      } else {
        console.error('GET EVENTS ERROR', resp);
        this.presentToast('Error al obtener los Eventos.');
      }
      if (event) {
        event.target.complete();
      }
    }, err => {
      console.error('GET EVENTS SERVER ERROR', err);
      this.presentToast('Error de Conexión');
      if (event) {
        event.target.complete();
      }
    });
  }

  viewDetails(id) {
    this.router.navigate(['private', 'events', 'detail', id]);
  }

  orderBy(prop: string) {
    return this.events.sort((a, b) => a[prop] > b[prop] ? 1 : a[prop] === b[prop] ? 0 : -1);
  }

  async presentToast(text) {
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

}
