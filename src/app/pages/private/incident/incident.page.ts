import { Component, OnInit } from '@angular/core';
import { IncidentService } from 'src/app/services/incidents/incident.service';
import { Storage } from '@ionic/storage';
import { ActivatedRoute, Router } from '@angular/router';
import { ConfigService } from 'src/app/services/config.service';
import { Geolocation, Geoposition } from '@ionic-native/geolocation/ngx';
import { Observable } from 'rxjs';
import * as CryptoJS from 'crypto-js';
declare var CryptoJSAesJson;


@Component({
  selector: 'app-incident',
  templateUrl: './incident.page.html',
  styleUrls: ['./incident.page.scss'],
})
export class IncidentPage implements OnInit {
  public incident: Incidents.IncidentInterface;
  public incidentLocation: Incidents.IncidentLocationInterface;
  public location: string;
  public mapsKey: string;
  public watch: Observable<Geoposition>;
  constructor(private incidentService: IncidentService,
    private route: ActivatedRoute,
    private router: Router,
    private configService: ConfigService,
    private storage: Storage,
    private geolocation: Geolocation) {
      this.incident = {
        nIdIncidente: 0,
        IdUsuario: 0,
        Tipo: this.route.snapshot.params.tipo
      } as Incidents.IncidentInterface;
      this.incidentLocation = {
        nIdIncidenteUbicacion: 0,
        IdIncidente: 0,
        Latitud: 0,
        Longitud: 0,
        action: 'ubicacion'
      } as Incidents.IncidentLocationInterface;

      this.location = '0,0';
      this.mapsKey = this.configService.mapsKey();
  }
  ngOnInit() {
    this.storage.get('user').then(usr => {
      var decrypted = JSON.parse(CryptoJS.AES.decrypt(usr, " ", {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
      console.log(decrypted);
      this.incident.IdUsuario = decrypted.nIDUsuario;
      this.incidentService.report(this.incident).subscribe(resp => {
        if (resp.status) {
          this.incidentLocation.IdIncidente = resp.IdIncidente;
          console.log('INCIDENT SAVED', resp);
        } else {
          console.error('INCIDENT SAVING ERROR', resp);//si no se realiza el registro del incidente
        }
      }, err => {
        console.error('ERROR SAVING INCIDENT', err);// si no se realiza la peticion para guardar el incidente al servidor
      });
    });




//ENVIAR LA LOCALIZACION PARA EL REGISTRO DE LA UBICACION DEL INCIDENTE
    this.watch = this.geolocation.watchPosition();
    this.watch.subscribe(position => {
      this.incidentLocation.Longitud = position.coords.longitude;//asignamos la longitud a insident location
      this.incidentLocation.Latitud = position.coords.latitude;//asignamos la latitud a insident location
      if (this.incidentLocation.IdIncidente !== 0) {
        this.location = position.coords.latitude + ',' + position.coords.longitude;
        this.incidentService.sendLocation(this.incidentLocation).subscribe(resp => {//enviamos la longitud y a latitud a la funcion sendLocation en el archivo incident.service
          console.log('SAVE LOCATION', resp);
          if (!resp.status) {
            console.error('Error2 SAVING LOCATION ERROR', resp);
          }
        }, err => {
          console.error('Error3 ERROR SAVE LOCATION', err);
        });
      }
    }, err => {
      console.error('Error4 ERROR GEOLOCATION', err);
    });
  }
  //otras funciones del esta seccion
  salir() {
    this.incidentLocation.IdIncidente = 0;
    this.router.navigate(['private', 'dashboard']);
  }
  ionViewWillLeave() {
    console.log('LEAVING!!');
    this.incidentLocation.IdIncidente = 0;
  }
}
