import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { PrivatePagePage } from './private-page.page';
import { PrivatePagesRoutingModule } from './private-pages-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PrivatePagesRoutingModule
  ],
  declarations: [PrivatePagePage]
})
export class PrivatePagePageModule {}
