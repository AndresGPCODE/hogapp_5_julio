import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { PrivatePagePage } from './private-page.page';

describe('PrivatePagePage', () => {
  let component: PrivatePagePage;
  let fixture: ComponentFixture<PrivatePagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PrivatePagePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PrivatePagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
