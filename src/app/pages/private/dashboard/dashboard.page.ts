import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],
})
export class DashboardPage implements OnInit {

  constructor(private router: Router, private alertController: AlertController) { }

  ngOnInit() {
  }

  report(tipo) {
    this.presentAlertConfirm('Reportar', '¿Está seguro que desea reportar ' + (tipo === 'Avería' ? 'una Avería' : 'un Accidente') + '?', [
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary'
      }, {
        text: 'Reportar',
        handler: () => {
          this.router.navigate(['private', 'incident', tipo]);
        }
      }
    ]);
  }

  async presentAlertConfirm(header: string, message: string, buttons: Array<any>) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: buttons
    });

    await alert.present();
  }
}
