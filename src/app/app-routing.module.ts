import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PrivateLayoutComponent } from './layouts/private-layout/private-layout.component';
import { PublicLayoutComponent } from './layouts/public-layout/public-layout.component';


export const PUBLIC_ROUTES: Routes = [
  {
    path: 'public',
    loadChildren: './pages/public/public-page.module#PublicPagePageModule'
  }
];

export const PRIVATE_ROUTES: Routes = [
  {
    path: 'private',
    loadChildren: './pages/private/private-page.module#PrivatePagePageModule'
  }
];

const routes: Routes = [
  {
    path: '',
    redirectTo: 'public/login',
    pathMatch: 'full'
  },
  { path: '', component: PublicLayoutComponent, children: PUBLIC_ROUTES},
  { path: '', component: PrivateLayoutComponent, children: PRIVATE_ROUTES},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
