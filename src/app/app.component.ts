import { Component } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { FcmService } from './services/firebase/fcm.service';

import * as CryptoJS from 'crypto-js';
declare var CryptoJSAesJson;

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Home',
      url: '/home',
      icon: 'home'
    },
    {
      title: 'List',
      url: '/list',
      icon: 'list'
    }
  ];
  showSplash = true;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private fcm: FcmService
  ) {
    this.initializeApp();
  }
  private async presentToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 3000
    });
    toast.present();
  }
  private notificationSetup(idUser) {
    this.fcm.getToken(idUser);
    this.fcm.onNotifications().subscribe(
      (msg) => {
        if (this.platform.is('ios')) {
          this.presentToast(msg.aps.alert);
        } else {
          this.presentToast(msg.body);
        }
      });
  }
  initializeApp() { 
    this.platform.ready().then(() => {
      // this.statusBar.styleDefault();
      this.statusBar.styleBlackTranslucent();
      this.splashScreen.hide();
      this.storage.get('user').then(usr => {

        //desecriptacion
        var decrypted = JSON.parse(CryptoJS.AES.decrypt(usr, " ", {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
        if (usr) {
          this.notificationSetup(decrypted.nIDUsuario);
          this.router.navigate(['private', 'dashboard'], { replaceUrl: true });
        }
      });
    });
  }
}