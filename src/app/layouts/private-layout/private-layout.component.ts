import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import * as CryptoJS from 'crypto-js';

declare var CryptoJSAesJson;

@Component({
  selector: 'app-private-layout',
  templateUrl: './private-layout.component.html',
  styleUrls: ['./private-layout.component.scss'],
})
export class PrivateLayoutComponent implements OnInit {
  public appPages = [
    {
      title: 'Inicio',
      url: '/private/dashboard',
      icon: 'home'
    },
    {
      title: 'Eventos',
      url: '/private/events',
      icon: 'calendar'
    }
    // ,
    // {
    //   title: 'Configuración',
    //   url: '/private/incident/simon',
    //   icon: 'cog'
    // }
    // ,
    // {
    //   title: 'Información Moto',
    //   url: '/private/vehicle',
    //   icon: 'bicycle'
    // },
    // {
    //   title: 'Datos Médicos',
    //   url: '/private/medical',
    //   icon: 'medkit'
    // }
  ];

  public userName: string;

  constructor(private storage: Storage, private router: Router, private alertController: AlertController) {
    this.storage.get('user').then(usr => {
      var decrypted = JSON.parse(CryptoJS.AES.decrypt(usr, " ", {format: CryptoJSAesJson}).toString(CryptoJS.enc.Utf8));
      if (usr) {
        this.userName = decrypted.Nombre;
      } else {
        this.userName = 'Usuario';
      }
    });
  }

  ngOnInit() {}

  logout() {
    this.presentAlertConfirm('Salir', '¿Está seguro que desea salir?', [
      {
        text: 'Cancelar',
        role: 'cancel',
        cssClass: 'secondary'
      }, {
        text: 'Salir',
        handler: () => {
          this.storage.clear();
          this.router.navigate(['public', 'login'], { replaceUrl: true });
        }
      }
    ]);
  }

  async presentAlertConfirm(header: string, message: string, buttons: Array<any>) {
    const alert = await this.alertController.create({
      header: header,
      message: message,
      buttons: buttons
    });

    await alert.present();
  }

}
