import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class ConfigService {
  private _host = 'https://www.estampidaapp.com/_API/';
  private _httpOptions = {
    headers: new HttpHeaders({
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Headers': '*'
  })
  };
  private _mapsKey = 'AIzaSyA7XfNi9Wz7prS7iIR2UizeA6N9h9LU_BU';

  constructor() { }

  host(val = null) {
    if (val == null) {
      return this._host;
    } else {
      this._host = val;
    }
  }

  headers() {
    return this._httpOptions;
  }

  mapsKey() {
    return this._mapsKey;
  }
}
