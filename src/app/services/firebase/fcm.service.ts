import { Injectable } from '@angular/core';
import { Firebase } from '@ionic-native/firebase/ngx';
import { Platform, ToastController } from '@ionic/angular';
import { AuthenticationService } from '../auth/authentication.service';

@Injectable({
  providedIn: 'root'
})
export class FcmService {

  constructor(private firebase: Firebase,
    private platform: Platform,
    private authService: AuthenticationService,
    private toastController: ToastController) { }

  async getToken(idUser) {
    let token;
    //si la plataforma es android es android
    if (this.platform.is('android')) {
      token = await this.firebase.getToken();
    }

    if (this.platform.is('ios')) {
      console.log('plataforma ios');
      token = await this.firebase.getToken();
      await this.firebase.grantPermission();
      console.log(token);
    }
    if(!this.platform.is('cordova')){
    }
    this.saveToken(token, idUser);
  }

  private saveToken(token, idUser) {
    if (!token) {
      return;
    }
    
    this.authService.saveFcmToken(idUser, token).subscribe(resp => {
      if (!resp.status) {
        console.error('ERROR FCM TOKEN', resp);
        this.presentToast('ERROR1' + resp);
      }
    }, err => {
      console.error('SERVER ERROR FCM TOKEN', err);
      this.presentToast('ERROR2' + err);
    });

    return;
  }

  async presentToast(text) {
    console.log('presenttoast token async');
    const toast = await this.toastController.create({
      message: text,
      duration: 2000
    });
    toast.present();
  }

  onNotifications() {
    return this.firebase.onNotificationOpen();
  }
}