import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import * as CryptoJS from 'crypto-js';
declare var CryptoJSAesJson;
const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  authenticationState = new BehaviorSubject(false);
  constructor(private storage: Storage, private plt: Platform, private http: HttpClient, private configService: ConfigService) {
    this.plt.ready().then(() => {
      this.checkToken();
    });
  }
    login(email, password) :Observable<any> {
      var encrypted = CryptoJS.AES.encrypt(JSON.stringify({email,password}), " ", {format: CryptoJSAesJson}).toString();
      return this.http.post(this.configService.host().concat('login.php'), encrypted,{});
  }
    logout() {
      return this.storage.remove(TOKEN_KEY).then(() => {
        this.authenticationState.next(false);
      });
    }
    isAuthenticated() { 
      return this.authenticationState.value;
    }
    checkToken() {
      return this.storage.get(TOKEN_KEY).then(resp => {
        if (resp) {
          this.authenticationState.next(true);
        }
      });
    }
    saveFcmToken(IdUsuario, TokenFCM):Observable<any>{
      var encrypted = CryptoJS.AES.encrypt(JSON.stringify({IdUsuario:IdUsuario,TokenFCM:TokenFCM}), " ", {format: CryptoJSAesJson}).toString();
      return this.http.post(
        this.configService.host().concat('fcm.php'),encrypted,{}
      );
    }
}