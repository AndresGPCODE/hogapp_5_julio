import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { Observable } from 'rxjs';
import * as CryptoJS from 'crypto-js';
declare var CryptoJSAesJson;

@Injectable({
  providedIn: 'root'
})
export class EventsService {
  constructor(private http: HttpClient, private config: ConfigService) { }

  get(): Observable<any> {
    return this.http.get(this.config.host().concat('accountEventos.php?bEstado=1&Today=1'),{});
  }

  getById(id): Observable<any> {
    return this.http.get(this.config.host().concat('accountEventos.php?bEstado=1&id=', id), {});
  }

  attend(attend: Events.AttendInterface): Observable<any> {
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify({attend}), " ", {format: CryptoJSAesJson}).toString();
    return this.http.post(this.config.host().concat('asistencias.php'), encrypted, {});
    // return this.http.post(this.config.host().concat('asistencias.php'), JSON.stringify({attend}), {});
  }
}