import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ConfigService } from '../config.service';
import { Observable } from 'rxjs';
import * as CryptoJS from 'crypto-js';
declare var CryptoJSAesJson;

@Injectable({
  providedIn: 'root'
})
export class IncidentService {
  constructor(private http: HttpClient, private config: ConfigService) { }

  report(incident: Incidents.IncidentInterface): Observable<any> {
    console.log('TO REPORT', incident);
    incident.action = 'reporte';
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify({incident}), " ", {format: CryptoJSAesJson}).toString();
    return this.http.post(this.config.host().concat('incidentes.php'), encrypted,{});
  }

  sendLocation(location: Incidents.IncidentLocationInterface): Observable<any> {
    console.log('SEND LOCATION', location);
    location.action = 'location';
    var encrypted = CryptoJS.AES.encrypt(JSON.stringify({location}), " ", {format: CryptoJSAesJson}).toString();
    return this.http.post(this.config.host().concat('incidentes.php'), encrypted,{});
  }
}