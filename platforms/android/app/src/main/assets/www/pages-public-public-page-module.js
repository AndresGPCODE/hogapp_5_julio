(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-public-public-page-module"],{

/***/ "./src/app/pages/public/public-page.module.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/public/public-page.module.ts ***!
  \****************************************************/
/*! exports provided: PublicPagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicPagePageModule", function() { return PublicPagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _public_page_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./public-page.page */ "./src/app/pages/public/public-page.page.ts");
/* harmony import */ var _public_pages_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./public-pages-routing.module */ "./src/app/pages/public/public-pages-routing.module.ts");







var PublicPagePageModule = /** @class */ (function () {
    function PublicPagePageModule() {
    }
    PublicPagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _public_pages_routing_module__WEBPACK_IMPORTED_MODULE_6__["PublicPagesRoutingModule"]
            ],
            declarations: [_public_page_page__WEBPACK_IMPORTED_MODULE_5__["PublicPagePage"]]
        })
    ], PublicPagePageModule);
    return PublicPagePageModule;
}());



/***/ }),

/***/ "./src/app/pages/public/public-page.page.html":
/*!****************************************************!*\
  !*** ./src/app/pages/public/public-page.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>publicPage</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/public/public-page.page.scss":
/*!****************************************************!*\
  !*** ./src/app/pages/public/public-page.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3B1YmxpYy9wdWJsaWMtcGFnZS5wYWdlLnNjc3MifQ== */"

/***/ }),

/***/ "./src/app/pages/public/public-page.page.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/public/public-page.page.ts ***!
  \**************************************************/
/*! exports provided: PublicPagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicPagePage", function() { return PublicPagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PublicPagePage = /** @class */ (function () {
    function PublicPagePage() {
    }
    PublicPagePage.prototype.ngOnInit = function () {
    };
    PublicPagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-public-page',
            template: __webpack_require__(/*! ./public-page.page.html */ "./src/app/pages/public/public-page.page.html"),
            styles: [__webpack_require__(/*! ./public-page.page.scss */ "./src/app/pages/public/public-page.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PublicPagePage);
    return PublicPagePage;
}());



/***/ }),

/***/ "./src/app/pages/public/public-pages-routing.module.ts":
/*!*************************************************************!*\
  !*** ./src/app/pages/public/public-pages-routing.module.ts ***!
  \*************************************************************/
/*! exports provided: PublicPagesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PublicPagesRoutingModule", function() { return PublicPagesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var routes = [
    { path: '', redirectTo: 'public/login' },
    { path: 'login', loadChildren: './login/login.module#LoginPageModule' },
    { path: 'register', loadChildren: './register/register.module#RegisterPageModule' }
];
var PublicPagesRoutingModule = /** @class */ (function () {
    function PublicPagesRoutingModule() {
    }
    PublicPagesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ]
        })
    ], PublicPagesRoutingModule);
    return PublicPagesRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-public-public-page-module.js.map