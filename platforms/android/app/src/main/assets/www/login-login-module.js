(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["login-login-module"],{

/***/ "./src/app/pages/public/login/login.module.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/public/login/login.module.ts ***!
  \****************************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/public/login/login.page.ts");







var routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
var LoginPageModule = /** @class */ (function () {
    function LoginPageModule() {
    }
    LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
        })
    ], LoginPageModule);
    return LoginPageModule;
}());



/***/ }),

/***/ "./src/app/pages/public/login/login.page.html":
/*!****************************************************!*\
  !*** ./src/app/pages/public/login/login.page.html ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-content padding>\n    <ion-grid>\n      <ion-row justify-content-center>\n        <ion-col align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          <div text-center>\n            <img src=\"../../../../assets/images/logo.png\">\n          </div>\n          <form name=\"loginForm\" #f=\"ngForm\" novalidate>\n            <div padding>\n              <ion-item>\n                <ion-input name=\"user\" type=\"email\" #email=\"ngModel\" placeholder=\"Usuario\" [(ngModel)]=\"user.email\" [ngClass]=\"{ 'is-invalid': email.invalid }\" required email></ion-input>\n              </ion-item>\n              <div *ngIf=\"email.invalid && email.dirty\" class=\"invalid-feedback\" padding>\n                  <div *ngIf=\"email.errors.required\">El Email es requerido</div>\n                  <div *ngIf=\"email.errors.email\">El Email no es válido</div>\n              </div>\n\n              <ion-item>\n                <ion-input name=\"password\" type=\"password\" placeholder=\"\" [(ngModel)]=\"user.password\" #password=\"ngModel\" [ngClass]=\"{ 'is-invalid': password.invalid }\" value=\"null\" required></ion-input>\n              </ion-item>\n              <div *ngIf=\"password.invalid && password.dirty\" class=\"invalid-feedback\" padding>\n                  <div *ngIf=\"password.errors.required\">El Password es requerido</div>\n              </div>\n            </div>\n\n            <div padding>\n              <ion-button expand=\"block\" (click)=\"login(user)\" [disabled]=\"email.invalid || password.invalid\">Ingresar</ion-button>\n              <ion-button color=\"secondary\" href=\"/public/register\" expand=\"block\">Registrarme</ion-button>\n            </div>\n          </form>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/public/login/login.page.scss":
/*!****************************************************!*\
  !*** ./src/app/pages/public/login/login.page.scss ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".invalid-feedback {\n  background-color: #e35f5f;\n  color: white;\n  border-radius: 15px;\n  margin-top: 5px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy91c3VhcmlvL0Rlc2t0b3AvSG9nQXBwXzI3X0pVTklPXzIwMTkvc3JjL2FwcC9wYWdlcy9wdWJsaWMvbG9naW4vbG9naW4ucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0VBQ0kseUJBQWtDO0VBQ2xDLFlBQVk7RUFDWixtQkFBbUI7RUFDbkIsZUFBZSxFQUFBIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvcHVibGljL2xvZ2luL2xvZ2luLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5pbnZhbGlkLWZlZWRiYWNre1xuICAgIGJhY2tncm91bmQtY29sb3I6IHJnYigyMjcsIDk1LCA5NSk7XG4gICAgY29sb3I6IHdoaXRlO1xuICAgIGJvcmRlci1yYWRpdXM6IDE1cHg7XG4gICAgbWFyZ2luLXRvcDogNXB4O1xufSJdfQ== */"

/***/ }),

/***/ "./src/app/pages/public/login/login.page.ts":
/*!**************************************************!*\
  !*** ./src/app/pages/public/login/login.page.ts ***!
  \**************************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_auth_authentication_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! src/app/services/auth/authentication.service */ "./src/app/services/auth/authentication.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var src_app_services_firebase_fcm_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! src/app/services/firebase/fcm.service */ "./src/app/services/firebase/fcm.service.ts");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_7__);








var LoginPage = /** @class */ (function () {
    function LoginPage(router, authService, toastController, storage, fcm, platform) {
        this.router = router;
        this.authService = authService;
        this.toastController = toastController;
        this.storage = storage;
        this.fcm = fcm;
        this.platform = platform;
        this.user = {
            email: '',
            password: ''
        };
    }
    LoginPage.prototype.ngOnInit = function () {
    };
    LoginPage.prototype.login = function (user) {
        var _this = this;
        this.authService.login(user.email, user.password).subscribe(function (resp) {
            if (resp.status) {
                //encriptacion de los datos de sesion
                resp.data = { 'Alias': resp.data['Alias'],
                    'Nombre': resp.data['Nombre'],
                    'Password': resp.data['Password'],
                    'nIDUsuario': resp.data['nIDUsuario'],
                    'nIDMotoClub': resp.data['nIDMotoClub  ']
                };
                var encrypted = crypto_js__WEBPACK_IMPORTED_MODULE_7__["AES"].encrypt(JSON.stringify(resp.data), " ", { format: CryptoJSAesJson }).toString();
                _this.storage.set('user', encrypted);
                var decrypted = JSON.parse(crypto_js__WEBPACK_IMPORTED_MODULE_7__["AES"].decrypt(encrypted, " ", { format: CryptoJSAesJson }).toString(crypto_js__WEBPACK_IMPORTED_MODULE_7__["enc"].Utf8));
                //---------------
                _this.presentToast('¡Bienvenido '.concat(decrypted.Nombre, '!'));
                _this.notificationSetup(decrypted.nIDUsuario);
                _this.router.navigate(['private', 'dashboard'], { replaceUrl: true });
            }
            else {
                console.log('LOGIN FAIL', resp);
                _this.presentToast(resp.message);
            }
        }, function (err) {
            console.error('ERROR LOGIN', err);
            _this.presentToast('Error de conexion');
        });
    };
    LoginPage.prototype.notificationSetup = function (idUser) {
        var _this = this;
        this.fcm.getToken(idUser);
        this.fcm.onNotifications().subscribe(function (msg) {
            if (_this.platform.is('ios')) {
                _this.presentToast(msg.aps.alert);
            }
            else {
                _this.presentToast(msg.body);
            }
        });
    };
    LoginPage.prototype.presentToast = function (text) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function () {
            var toast;
            return tslib__WEBPACK_IMPORTED_MODULE_0__["__generator"](this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.toastController.create({
                            message: text,
                            duration: 2000
                        })];
                    case 1:
                        toast = _a.sent();
                        toast.present();
                        return [2 /*return*/];
                }
            });
        });
    };
    LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.page.html */ "./src/app/pages/public/login/login.page.html"),
            styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/public/login/login.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"],
            src_app_services_auth_authentication_service__WEBPACK_IMPORTED_MODULE_3__["AuthenticationService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["ToastController"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_5__["Storage"],
            src_app_services_firebase_fcm_service__WEBPACK_IMPORTED_MODULE_6__["FcmService"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["Platform"]])
    ], LoginPage);
    return LoginPage;
}());



/***/ })

}]);
//# sourceMappingURL=login-login-module.js.map