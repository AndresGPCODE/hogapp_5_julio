(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-private-private-page-module"],{

/***/ "./src/app/pages/private/private-page.module.ts":
/*!******************************************************!*\
  !*** ./src/app/pages/private/private-page.module.ts ***!
  \******************************************************/
/*! exports provided: PrivatePagePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivatePagePageModule", function() { return PrivatePagePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _private_page_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./private-page.page */ "./src/app/pages/private/private-page.page.ts");
/* harmony import */ var _private_pages_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./private-pages-routing.module */ "./src/app/pages/private/private-pages-routing.module.ts");







var PrivatePagePageModule = /** @class */ (function () {
    function PrivatePagePageModule() {
    }
    PrivatePagePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonicModule"],
                _private_pages_routing_module__WEBPACK_IMPORTED_MODULE_6__["PrivatePagesRoutingModule"]
            ],
            declarations: [_private_page_page__WEBPACK_IMPORTED_MODULE_5__["PrivatePagePage"]]
        })
    ], PrivatePagePageModule);
    return PrivatePagePageModule;
}());



/***/ }),

/***/ "./src/app/pages/private/private-page.page.html":
/*!******************************************************!*\
  !*** ./src/app/pages/private/private-page.page.html ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-title>privatePage</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/private/private-page.page.scss":
/*!******************************************************!*\
  !*** ./src/app/pages/private/private-page.page.scss ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ByaXZhdGUvcHJpdmF0ZS1wYWdlLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/private/private-page.page.ts":
/*!****************************************************!*\
  !*** ./src/app/pages/private/private-page.page.ts ***!
  \****************************************************/
/*! exports provided: PrivatePagePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivatePagePage", function() { return PrivatePagePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var PrivatePagePage = /** @class */ (function () {
    function PrivatePagePage() {
    }
    PrivatePagePage.prototype.ngOnInit = function () {
    };
    PrivatePagePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-private-page',
            template: __webpack_require__(/*! ./private-page.page.html */ "./src/app/pages/private/private-page.page.html"),
            styles: [__webpack_require__(/*! ./private-page.page.scss */ "./src/app/pages/private/private-page.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], PrivatePagePage);
    return PrivatePagePage;
}());



/***/ }),

/***/ "./src/app/pages/private/private-pages-routing.module.ts":
/*!***************************************************************!*\
  !*** ./src/app/pages/private/private-pages-routing.module.ts ***!
  \***************************************************************/
/*! exports provided: PrivatePagesRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PrivatePagesRoutingModule", function() { return PrivatePagesRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var routes = [
    { path: '', redirectTo: 'private/dashboard' },
    { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
    { path: 'events', loadChildren: './events/events.module#EventsPageModule' },
    { path: 'configuration', loadChildren: './configuration/configuration.module#ConfigurationPageModule' },
    { path: 'vehicle', loadChildren: './vehicle/vehicle.module#VehiclePageModule' },
    { path: 'medical', loadChildren: './medical/medical.module#MedicalPageModule' },
    { path: 'incident/:tipo', loadChildren: './incident/incident.module#IncidentPageModule' }
    // { path: 'attend', loadChildren: './events/attend/attend.module#AttendPageModule' },
    // { path: 'details', loadChildren: './events/details/details.module#DetailsPageModule' }
];
var PrivatePagesRoutingModule = /** @class */ (function () {
    function PrivatePagesRoutingModule() {
    }
    PrivatePagesRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [],
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_3__["RouterModule"].forChild(routes)
            ]
        })
    ], PrivatePagesRoutingModule);
    return PrivatePagesRoutingModule;
}());



/***/ })

}]);
//# sourceMappingURL=pages-private-private-page-module.js.map