(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["vehicle-vehicle-module"],{

/***/ "./src/app/pages/private/vehicle/vehicle.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/private/vehicle/vehicle.module.ts ***!
  \*********************************************************/
/*! exports provided: VehiclePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehiclePageModule", function() { return VehiclePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _vehicle_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./vehicle.page */ "./src/app/pages/private/vehicle/vehicle.page.ts");







var routes = [
    {
        path: '',
        component: _vehicle_page__WEBPACK_IMPORTED_MODULE_6__["VehiclePage"]
    }
];
var VehiclePageModule = /** @class */ (function () {
    function VehiclePageModule() {
    }
    VehiclePageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_vehicle_page__WEBPACK_IMPORTED_MODULE_6__["VehiclePage"]]
        })
    ], VehiclePageModule);
    return VehiclePageModule;
}());



/***/ }),

/***/ "./src/app/pages/private/vehicle/vehicle.page.html":
/*!*********************************************************!*\
  !*** ./src/app/pages/private/vehicle/vehicle.page.html ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-toggle>\n        <ion-button>\n          <ion-icon slot=\"icon-only\" name=\"menu\"></ion-icon>\n        </ion-button>\n      </ion-menu-toggle>\n    </ion-buttons>\n    <ion-title>Mi Moto</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/private/vehicle/vehicle.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/private/vehicle/vehicle.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ByaXZhdGUvdmVoaWNsZS92ZWhpY2xlLnBhZ2Uuc2NzcyJ9 */"

/***/ }),

/***/ "./src/app/pages/private/vehicle/vehicle.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/private/vehicle/vehicle.page.ts ***!
  \*******************************************************/
/*! exports provided: VehiclePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "VehiclePage", function() { return VehiclePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var VehiclePage = /** @class */ (function () {
    function VehiclePage() {
    }
    VehiclePage.prototype.ngOnInit = function () {
    };
    VehiclePage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-vehicle',
            template: __webpack_require__(/*! ./vehicle.page.html */ "./src/app/pages/private/vehicle/vehicle.page.html"),
            styles: [__webpack_require__(/*! ./vehicle.page.scss */ "./src/app/pages/private/vehicle/vehicle.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], VehiclePage);
    return VehiclePage;
}());



/***/ })

}]);
//# sourceMappingURL=vehicle-vehicle-module.js.map