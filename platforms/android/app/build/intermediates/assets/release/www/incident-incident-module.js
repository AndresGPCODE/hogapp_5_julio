(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["incident-incident-module"],{

/***/ "./src/app/pages/private/incident/incident.module.ts":
/*!***********************************************************!*\
  !*** ./src/app/pages/private/incident/incident.module.ts ***!
  \***********************************************************/
/*! exports provided: IncidentPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncidentPageModule", function() { return IncidentPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm5/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _incident_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./incident.page */ "./src/app/pages/private/incident/incident.page.ts");







var routes = [
    {
        path: '',
        component: _incident_page__WEBPACK_IMPORTED_MODULE_6__["IncidentPage"]
    }
];
var IncidentPageModule = /** @class */ (function () {
    function IncidentPageModule() {
    }
    IncidentPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [
                _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
                _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
                _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
            ],
            declarations: [_incident_page__WEBPACK_IMPORTED_MODULE_6__["IncidentPage"]]
        })
    ], IncidentPageModule);
    return IncidentPageModule;
}());



/***/ }),

/***/ "./src/app/pages/private/incident/incident.page.html":
/*!***********************************************************!*\
  !*** ./src/app/pages/private/incident/incident.page.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header>\n  <ion-toolbar>\n    <ion-buttons slot=\"start\">\n      <ion-menu-toggle>\n        <ion-button>\n          <ion-icon slot=\"icon-only\" name=\"menu\"></ion-icon>\n        </ion-button>\n      </ion-menu-toggle>\n    </ion-buttons>\n    <ion-title>Reporte de {{ incident.Tipo }}</ion-title>\n  </ion-toolbar>\n</ion-header>\n\n<ion-content padding>\n  <div *ngIf=\"location != '0,0'\">\n    <img [src]=\"'https://maps.googleapis.com/maps/api/staticmap?center=' + location + '&zoom=15&size=600x300&maptype=roadmap&markers=color:red%7Clabel:C%7C' + location + '&key=' + mapsKey\">\n  </div>\n\n  <p text-center>Mientras estés en esta pantalla se está enviando tu ubicación a tus compañeros para que puedan acudir a ayudarte.</p>\n  <div padding>\n    <ion-button expand=\"block\" (click)=\"salir()\">Dejar de compartir</ion-button>\n  </div>\n</ion-content>\n"

/***/ }),

/***/ "./src/app/pages/private/incident/incident.page.scss":
/*!***********************************************************!*\
  !*** ./src/app/pages/private/incident/incident.page.scss ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL3ByaXZhdGUvaW5jaWRlbnQvaW5jaWRlbnQucGFnZS5zY3NzIn0= */"

/***/ }),

/***/ "./src/app/pages/private/incident/incident.page.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/private/incident/incident.page.ts ***!
  \*********************************************************/
/*! exports provided: IncidentPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncidentPage", function() { return IncidentPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var src_app_services_incidents_incident_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/services/incidents/incident.service */ "./src/app/services/incidents/incident.service.ts");
/* harmony import */ var _ionic_storage__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/storage */ "./node_modules/@ionic/storage/fesm5/ionic-storage.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var src_app_services_config_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @ionic-native/geolocation/ngx */ "./node_modules/@ionic-native/geolocation/ngx/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_7__);








var IncidentPage = /** @class */ (function () {
    function IncidentPage(incidentService, route, router, configService, storage, geolocation) {
        this.incidentService = incidentService;
        this.route = route;
        this.router = router;
        this.configService = configService;
        this.storage = storage;
        this.geolocation = geolocation;
        this.incident = {
            nIdIncidente: 0,
            IdUsuario: 0,
            Tipo: this.route.snapshot.params.tipo
        };
        this.incidentLocation = {
            nIdIncidenteUbicacion: 0,
            IdIncidente: 0,
            Latitud: 0,
            Longitud: 0,
            action: 'ubicacion'
        };
        this.location = '0,0';
        this.mapsKey = this.configService.mapsKey();
    }
    IncidentPage.prototype.ngOnInit = function () {
        var _this = this;
        this.storage.get('user').then(function (usr) {
            var decrypted = JSON.parse(crypto_js__WEBPACK_IMPORTED_MODULE_7__["AES"].decrypt(usr, " ", { format: CryptoJSAesJson }).toString(crypto_js__WEBPACK_IMPORTED_MODULE_7__["enc"].Utf8));
            console.log(decrypted);
            _this.incident.IdUsuario = decrypted.nIDUsuario;
            _this.incidentService.report(_this.incident).subscribe(function (resp) {
                if (resp.status) {
                    _this.incidentLocation.IdIncidente = resp.IdIncidente;
                    console.log('INCIDENT SAVED', resp);
                }
                else {
                    console.error('INCIDENT SAVING ERROR', resp); //si no se realiza el registro del incidente
                }
            }, function (err) {
                console.error('ERROR SAVING INCIDENT', err); // si no se realiza la peticion para guardar el incidente al servidor
            });
        });
        //ENVIAR LA LOCALIZACION PARA EL REGISTRO DE LA UBICACION DEL INCIDENTE
        this.watch = this.geolocation.watchPosition();
        this.watch.subscribe(function (position) {
            _this.incidentLocation.Longitud = position.coords.longitude; //asignamos la longitud a insident location
            _this.incidentLocation.Latitud = position.coords.latitude; //asignamos la latitud a insident location
            if (_this.incidentLocation.IdIncidente !== 0) {
                _this.location = position.coords.latitude + ',' + position.coords.longitude;
                _this.incidentService.sendLocation(_this.incidentLocation).subscribe(function (resp) {
                    console.log('SAVE LOCATION', resp);
                    if (!resp.status) {
                        console.error('Error2 SAVING LOCATION ERROR', resp);
                    }
                }, function (err) {
                    console.error('Error3 ERROR SAVE LOCATION', err);
                });
            }
        }, function (err) {
            console.error('Error4 ERROR GEOLOCATION', err);
        });
    };
    //otras funciones del esta seccion
    IncidentPage.prototype.salir = function () {
        this.incidentLocation.IdIncidente = 0;
        this.router.navigate(['private', 'dashboard']);
    };
    IncidentPage.prototype.ionViewWillLeave = function () {
        console.log('LEAVING!!');
        this.incidentLocation.IdIncidente = 0;
    };
    IncidentPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-incident',
            template: __webpack_require__(/*! ./incident.page.html */ "./src/app/pages/private/incident/incident.page.html"),
            styles: [__webpack_require__(/*! ./incident.page.scss */ "./src/app/pages/private/incident/incident.page.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_services_incidents_incident_service__WEBPACK_IMPORTED_MODULE_2__["IncidentService"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["ActivatedRoute"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"],
            src_app_services_config_service__WEBPACK_IMPORTED_MODULE_5__["ConfigService"],
            _ionic_storage__WEBPACK_IMPORTED_MODULE_3__["Storage"],
            _ionic_native_geolocation_ngx__WEBPACK_IMPORTED_MODULE_6__["Geolocation"]])
    ], IncidentPage);
    return IncidentPage;
}());



/***/ }),

/***/ "./src/app/services/incidents/incident.service.ts":
/*!********************************************************!*\
  !*** ./src/app/services/incidents/incident.service.ts ***!
  \********************************************************/
/*! exports provided: IncidentService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncidentService", function() { return IncidentService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _config_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../config.service */ "./src/app/services/config.service.ts");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! crypto-js */ "./node_modules/crypto-js/index.js");
/* harmony import */ var crypto_js__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(crypto_js__WEBPACK_IMPORTED_MODULE_4__);





var IncidentService = /** @class */ (function () {
    function IncidentService(http, config) {
        this.http = http;
        this.config = config;
    }
    IncidentService.prototype.report = function (incident) {
        console.log('TO REPORT', incident);
        incident.action = 'reporte';
        var encrypted = crypto_js__WEBPACK_IMPORTED_MODULE_4__["AES"].encrypt(JSON.stringify({ incident: incident }), " ", { format: CryptoJSAesJson }).toString();
        return this.http.post(this.config.host().concat('incidentes.php'), encrypted, {});
    };
    IncidentService.prototype.sendLocation = function (location) {
        console.log('SEND LOCATION', location);
        location.action = 'location';
        var encrypted = crypto_js__WEBPACK_IMPORTED_MODULE_4__["AES"].encrypt(JSON.stringify({ location: location }), " ", { format: CryptoJSAesJson }).toString();
        return this.http.post(this.config.host().concat('incidentes.php'), encrypted, {});
    };
    IncidentService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _config_service__WEBPACK_IMPORTED_MODULE_3__["ConfigService"]])
    ], IncidentService);
    return IncidentService;
}());



/***/ })

}]);
//# sourceMappingURL=incident-incident-module.js.map