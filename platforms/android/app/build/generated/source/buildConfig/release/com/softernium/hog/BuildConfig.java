/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.softernium.hog;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "com.softernium.hog";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 100;
  public static final String VERSION_NAME = "0.1.0";
}
